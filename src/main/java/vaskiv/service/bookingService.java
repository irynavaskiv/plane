package vaskiv.service;

import vaskiv.annotation.bookingHead;
import vaskiv.performance.bookingDao;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("ALL")
public class bookingService {
    public List<bookingHead> findAll() throws SQLException {
        return new bookingDao().findAll();
    }
    public List<bookingHead> findById() throws SQLException{
        return (bookingHead) new bookingDao().findById();
    }
    public List<bookingHead> findByHourBooking() throws SQLException{
        return (bookingHead) new bookingDao().findByHourBooking();
    }
    public bookingHead findByQuantity() throws SQLException{
        return (bookingHead) new bookingDao().findByQuantity();
    }
}
