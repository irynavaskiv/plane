package vaskiv.service;

import vaskiv.annotation.flightsHead;
import vaskiv.performance.flightsDao;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("ALL")
public class flightsService {
    public List<flightsHead> findAll() throws SQLException {
        return new flightsDao().findAll();
    }
    public flightsHead findById() throws SQLException{
        return (flightsHead) new flightsDao().findById();
    }
    public flightsHead findByTrip() throws SQLException{
        return (flightsHead) new flightsDao().findByTrip();
    }
    public flightsHead findByTimeFlights() throws SQLException{
        return (flightsHead) new flightsDao().findByTimeFlights();
    }
    public flightsHead findByNumberFlights() throws SQLException{
        return (flightsHead) new flightsDao().findByNumberFlights();
    }
}
