package vaskiv.annotation;

public class planeHead {

    private  String id;
    private  String name_plane;
    private  String motor;
    private  String speed;

    public planeHead(String id, String name_plane, String motor, String speed){
        this.id = id;
        this.name_plane = name_plane;
        this.motor = motor;
        this.speed = speed;
    }
    public String getId() { return id;  }

    public void setId(String id) {  this.id = id; }

    public String getName_plane() {
        return name_plane;
    }

    public void setName_plane(String name_plane) {
        this.name_plane = name_plane;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) { this.motor = motor; }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
