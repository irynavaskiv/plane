package vaskiv.annotation;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class bookingHead implements List<bookingHead> {
    private String id;
    private String hour_booking;
    private String quantity;

    public bookingHead (String id, String hour_booking, String quantity){
        this.id = id;
        this.hour_booking=hour_booking;
        this.quantity = quantity;
    }

    public String getId() { return id;    }

    public void setId(String id) {  this.id = id;  }

    public String getHour_booking() {   return hour_booking; }

    public void setHour_booking(String hour_booking) {
        this.hour_booking = hour_booking;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<bookingHead> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return null;
    }

    @Override
    public boolean add(bookingHead bookingHead) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends bookingHead> collection) {
        return false;
    }

    @Override
    public boolean addAll(int i, Collection<? extends bookingHead> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public bookingHead get(int i) {
        return null;
    }

    @Override
    public bookingHead set(int i, bookingHead bookingHead) {
        return null;
    }

    @Override
    public void add(int i, bookingHead bookingHead) {

    }

    @Override
    public bookingHead remove(int i) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<bookingHead> listIterator() {
        return null;
    }

    @Override
    public ListIterator<bookingHead> listIterator(int i) {
        return null;
    }

    @Override
    public List<bookingHead> subList(int i, int i1) {
        return null;
    }
}
