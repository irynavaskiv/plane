package vaskiv.annotation;

public class clientsHead {
    private String id;
    private String name_surname;

    public clientsHead(String id, String name_surname){
        this.id=id;
        this.name_surname = name_surname;
    }
    public String getId() { return id; }

    public void setId(String id) { this.id = id;  }

    public clientsHead(String name_surname){
        this.name_surname=name_surname;
    }

    public String getName_surname() {  return name_surname;  }

    public void setName_surname(String name_surname) {
        this.name_surname = name_surname;
    }
}
