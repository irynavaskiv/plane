package vaskiv.annotation;

public class flightsHead {
    private String id;
    private String trip;
    private String time_flights;
    private String number_flights;

    public flightsHead(String id, String trip, String time_flights, String number_flights){

        this.id = id;
        this.trip = trip;
        this.time_flights=time_flights;
        this.number_flights = number_flights;
    }
    public String getId() { return id; }

    public void setId(String id) { this.id = id;}

    public String getTrip() {
        return trip;
    }

    public void setTrip(String trip) {
        this.trip = trip;
    }

    public String getTime_flights() {
        return time_flights;
    }

    public void setTime_flights(String time_flights) {
        this.time_flights = time_flights;
    }

    public String getNumber_flights() {
        return number_flights;
    }

    public void setNumber_flights(String number_flights) {
        this.number_flights = number_flights;
    }
}

