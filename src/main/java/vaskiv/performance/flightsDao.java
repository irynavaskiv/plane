package vaskiv.performance;

import vaskiv.annotation.flightsHead;
import vaskiv.connection.ConnectionWithBd;
import vaskiv.performance.interfaces.flightsDaoInterface;
import vaskiv.transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static vaskiv.consts.FlightsConstants.*;
import static vaskiv.consts.planeConstants.FIND_ALL;
import static vaskiv.consts.planeConstants.FIND_ID;

@SuppressWarnings("ALL")
public class flightsDao implements flightsDaoInterface {

    @Override
    public List<flightsHead> findAll() throws SQLException {
        List<flightsHead> flights = new ArrayList<flightsHead>();
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try(Statement statement = connection.createStatement()){
            try (ResultSet resultSet= statement.executeQuery(FIND_ALL)){
                while (resultSet.next()){
                    new transformer(flightsHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
            return flights;
        }
    }

    @Override
    public flightsHead findById() throws SQLException {
        flightsHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_ID)) {
            String id = null;
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(flightsHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }

    @Override
    public flightsHead findByTrip() throws SQLException {
        flightsHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_TRIP)) {
            String trip = null;
            ps.setString(1, trip);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(flightsHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }

    @Override
    public flightsHead findByTimeFlights() throws SQLException {
        flightsHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_TIME_FLIGHTS)) {
            String time_flights = null;
            ps.setString(1, time_flights);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(flightsHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }

    @Override
    public flightsHead findByNumberFlights() throws SQLException {
        flightsHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_NUMBER_FLIGHTS)) {
            String number_flights = null;
            ps.setString(1, number_flights);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(flightsHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }
}
