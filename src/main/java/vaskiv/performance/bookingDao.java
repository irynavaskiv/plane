package vaskiv.performance;

import vaskiv.annotation.bookingHead;
import vaskiv.connection.ConnectionWithBd;
import vaskiv.performance.interfaces.bookingDaoInterface;
import vaskiv.transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static vaskiv.consts.bookingConstants.FIND_HOUR_BOOKING;
import static vaskiv.consts.bookingConstants.FIND_QUANTITY;
import static vaskiv.consts.planeConstants.FIND_ALL;
import static vaskiv.consts.planeConstants.FIND_ID;

@SuppressWarnings("ALL")
public class bookingDao implements bookingDaoInterface {

    @Override
    public List<bookingHead> findAll() throws SQLException {
        List<bookingHead> booking = new ArrayList<bookingHead>();
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    new transformer(bookingHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
            return booking;
        }
    }

    @Override
    public bookingHead findById() throws SQLException {
        bookingHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_ID)) {
            String id = null;
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(bookingHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }

    @Override
    public bookingHead findByHourBooking() throws SQLException {
        bookingHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_HOUR_BOOKING)) {
            String hour_booking = null;
            ps.setString(1, hour_booking);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(bookingHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }

    @Override
    public bookingHead findByQuantity() throws SQLException {
        bookingHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_QUANTITY)) {
            String quantity = null;
            ps.setString(1, quantity);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(bookingHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }
}
