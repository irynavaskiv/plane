package vaskiv.performance.interfaces;

import vaskiv.annotation.planeHead;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("ALL")
public interface planeDaoInterface extends generalDaoInterface<planeHead, String> {

    List<planeHead> findAll() throws SQLException;

    planeHead findById(String s) throws SQLException;

    planeHead findByNamePlane(String s) throws SQLException;

    planeHead findByMotor(String s) throws SQLException;

    planeHead findBySpeed(String s) throws SQLException;
}
