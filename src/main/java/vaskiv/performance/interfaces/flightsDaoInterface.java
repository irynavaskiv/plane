package vaskiv.performance.interfaces;

import vaskiv.annotation.flightsHead;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("ALL")
public interface flightsDaoInterface extends generalDaoInterface<flightsDaoInterface, String> {
    flightsHead findByTimeFlights() throws SQLException;

    List<flightsHead> findAll() throws SQLException;

    flightsHead findById() throws SQLException;

    flightsHead findByTrip() throws SQLException;

    flightsHead findByNumberFlights() throws SQLException;
}
