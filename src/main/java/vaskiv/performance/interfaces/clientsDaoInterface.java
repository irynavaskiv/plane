package vaskiv.performance.interfaces;

import vaskiv.annotation.clientsHead;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("ALL")
public interface clientsDaoInterface extends generalDaoInterface<clientsHead, String> {
    clientsHead findByid(String s) throws SQLException;

    List<clientsHead> findAll() throws SQLException;

    clientsHead findById(String s) throws SQLException;

    clientsHead findByName_Surname(String s) throws SQLException;
}
