package vaskiv.performance.interfaces;

import vaskiv.annotation.bookingHead;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("ALL")
public interface bookingDaoInterface extends generalDaoInterface<bookingHead, String> {
    List<bookingHead> findAll() throws SQLException;

    bookingHead findById() throws SQLException;

    bookingHead findByHourBooking() throws SQLException;

    bookingHead findByQuantity() throws SQLException;
}
