package vaskiv.performance;

import vaskiv.annotation.planeHead;
import vaskiv.connection.ConnectionWithBd;
import vaskiv.consts.planeConstants;
import vaskiv.performance.interfaces.planeDaoInterface;
import vaskiv.transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static vaskiv.consts.planeConstants.*;

@SuppressWarnings("ALL")
public class planeDao implements planeDaoInterface {

    @Override
    public List<planeHead> findAll() throws SQLException {
        List<planeHead> flights = new ArrayList<>();
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try(Statement statement = connection.createStatement()){
            try (ResultSet resultSet= statement.executeQuery(planeConstants.FIND_ALL)){
                while (resultSet.next())
                    new transformer(planeHead.class).fromResultSetToHead(resultSet);
                    }
            return flights;
        }
    }
    @Override
    public planeHead findById(String s) throws SQLException {
        planeHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_ID)) {
            String id = null;
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(planeHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }
    @Override
    public planeHead findByNamePlane(String s) throws SQLException {
        planeHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_NAME_PLANE)) {
            String name_plane = null;
            ps.setString(1, name_plane);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(planeHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }

    @Override
    public planeHead findByMotor(String s) throws SQLException {
        planeHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_MOTOR)) {
            String motor = null;
            ps.setString(1, motor);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(planeHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }

    @Override
    public planeHead findBySpeed(String s) throws SQLException {
        planeHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_SPEED)) {
            String speed = null;
            ps.setString(1, speed);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(planeHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }
}


