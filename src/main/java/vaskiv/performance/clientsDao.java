package vaskiv.performance;

import vaskiv.annotation.clientsHead;
import vaskiv.connection.ConnectionWithBd;
import vaskiv.performance.interfaces.clientsDaoInterface;
import vaskiv.transformer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static vaskiv.consts.clientsConstants.FIND_NAME_SURNAME;
import static vaskiv.consts.planeConstants.FIND_ALL;
import static vaskiv.consts.planeConstants.FIND_ID;

@SuppressWarnings("ALL")
public class clientsDao implements clientsDaoInterface {

    @Override
    public List<clientsHead> findAll() throws SQLException {
        List<clientsHead> clients = new ArrayList<clientsHead>();
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    new transformer(clientsHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
            return clients;
        }
    }

    @Override
    public clientsHead findById(String s) throws SQLException {
        clientsHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_ID)) {
            String id = null;
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(clientsHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }

    @Override
    public clientsHead findByName_Surname(String s) throws SQLException {
        clientsHead head = null;
        ConnectionWithBd connection = ConnectionWithBd.getConnectionWithBd();
        try (PreparedStatement ps = connection.prepareStatement(FIND_NAME_SURNAME)) {
            String name_surname = null;
            ps.setString(1, name_surname);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    new transformer(clientsHead.class).fromResultSetToHead(resultSet);
                    break;
                }
            }
        }
        return head;
    }
    @Override
    public clientsHead findByid(String s) throws SQLException {
        return null;
    }
}
