package vaskiv.view;

import vaskiv.tablesView.*;

import java.util.LinkedHashMap;
import java.util.Map;
import static vaskiv.consts.logggerConstants.LOG;

public class mainView {
    private Map<String,String> menu;
    private Map<String, String> methodsMenu;
    private String exitFromProgram;

    private mainView(){
        initMenu();
    }
    private void initMenu() {
        menu =new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<String, String>();

        menu.put("1", " 1 - Take Full Structure of DB (metadata)");
        menu.put("2", " 2 - Select All tables");
        menu.put("3", " 3 - Select Table plane");
        menu.put("4", " 4 - Select Table flights");
        menu.put("5", " 5 - Select Table booking");
        menu.put("6", " 6 - Select Table clients");

        menu.put("31", " 31 - Find plane by id");
        menu.put("32", " 32 - Find plane by name of plane");
        menu.put("33", " 33 - Find plane by motor");
        menu.put("34", " 34 - Find plane by speed");

        menu.put("41", " 41 - Find flights by id");
        menu.put("42", " 42 - Find flights by trip");
        menu.put("43", " 43 - Find flights by time_flights");
        menu.put("44", " 44 - Find flights by number_flight");

        menu.put("51", " 51 - Find booking by id");
        menu.put("52", " 52 - Find booking by hour_booking");
        menu.put("53", " 53 - Find booking by quantity");

        menu.put("61", " 61 - Find clients by id");
        menu.put("62", " 62 - Find clients by name_surname");

        menu.put("0", " 0 - Exit.");
        
        methodsMenu.put("1", AllProgectView.takeStructureOfDB);
        methodsMenu.put("2", String.valueOf(new AllProgectView.selectAllTable()));
        methodsMenu.put("3", String.valueOf (new AllProgectView.selectPlane()));
        methodsMenu.put("4", String.valueOf(new AllProgectView.selectFlight()));
        methodsMenu.put("5", String.valueOf (new AllProgectView.selectBooking()));
        methodsMenu.put("6", String.valueOf(new AllProgectView.selectClients()));

        methodsMenu.put("31", String.valueOf(new PlaneView.findAll()));
        methodsMenu.put("32", String.valueOf(new PlaneView.findByNamePlane()));
        methodsMenu.put("34", String.valueOf(new PlaneView.findByMotor()));
        methodsMenu.put("35", String.valueOf(new PlaneView.findBySpeed()));

        methodsMenu.put("41", String.valueOf(new FlightsView.findAll()));
        methodsMenu.put("42", String.valueOf(new FlightsView.findById()));
        methodsMenu.put("43", String.valueOf(new FlightsView.findByTrip()));
        methodsMenu.put("44", String.valueOf(new FlightsView.findByTimeFlights()));
        methodsMenu.put("45", String.valueOf(new FlightsView.findByNumberFlights()));

        methodsMenu.put("51", String.valueOf(new BookingView.findAll()));
        methodsMenu.put("52", String.valueOf(new BookingView.findById()));
        methodsMenu.put("53", String.valueOf(new BookingView.findByHourBooking()));
        methodsMenu.put("54", String.valueOf(new BookingView.findByQuantity()));

        methodsMenu.put("61", String.valueOf(new ClientsView.findAll()));
        methodsMenu.put("62", String.valueOf(new ClientsView.findById()));
        methodsMenu.put("63", String.valueOf(new ClientsView.findByName_Surname()));

        methodsMenu.put("0", this.exitFromProgram);
    }
    private void outputMenu() {
        LOG.info("\nMENU:");
        menu.values().forEach(System.out::println);
    }
    private void exitFromProgram() {
        LOG.debug("SUCCESSFULLY END");
        System.exit(0);
    }
    public static void runMenu() {
        String key;
        mainView menu = new mainView();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                System.out.println("Please, select menu point");
                key = null;
                System.out.println("\n\n");
                menu.methodsMenu.get(key);
            } catch (NullPointerException e) {
               LOG.warn("\nChoose correct choice please");
            } catch (Exception e) {
                LOG.error("Error!");
                e.printStackTrace();
            }
        }
    }
}
