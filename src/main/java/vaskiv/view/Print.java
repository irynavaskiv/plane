package vaskiv.view;

import java.sql.SQLDataException;

@FunctionalInterface
public interface Print {
    void print() throws SQLDataException;
}
