package vaskiv.consts;

public class bookingConstants {
    private static final String FIND_ALL = "SELECT * FROM booking";
    private static final String FIND_ID = "SELECT * FROM booking WHERE id = ?";
    public static final String FIND_HOUR_BOOKING = "SELECT * FROM booking WHERE hour_booking = ?";
    public static final String FIND_QUANTITY = "SELECT * FROM booking WHERE quantity = ?";
}
