package vaskiv.consts;

public class planeConstants {
    public static final String FIND_ALL = "SELECT * FROM plane";
    public static final String FIND_ID = "SELECT * FROM plane WHERE id = ?";
    public static final String FIND_NAME_PLANE = "SELECT * FROM plane WHERE name_plane = ?";
    public static final String FIND_MOTOR = "SELECT * FROM plane WHERE motor = ?";
    public static final String FIND_SPEED = "SELECT * FROM plane WHERE speed = ?";

}
