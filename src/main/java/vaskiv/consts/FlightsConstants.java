package vaskiv.consts;

public class FlightsConstants {
    private static final String FIND_ALL = "SELECT * FROM flights";
    private static final String FIND_ID = "SELECT * FROM flights WHERE id = ?";
    public static final String FIND_TRIP = "SELECT * FROM flights WHERE trip = ?";
    public static final String FIND_TIME_FLIGHTS = "SELECT * FROM flights WHERE time_flights = ?";
    public static final String FIND_NUMBER_FLIGHTS = "SELECT * FROM flights WHERE number_flights = ?";
}
