package vaskiv.consts;

public class connectionConstants {
    public static final String url = "jdbc:mysql://localhost:3306/sample?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";
    public static final String user = "root";
    public static final String password = "vaskiv";
}
