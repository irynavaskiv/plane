package vaskiv.consts;

public class clientsConstants {
    private static final String FIND_ALL = "SELECT * FROM clients";
    private static final String FIND_ID = "SELECT * FROM clients WHERE id = ?";
    public static final String FIND_NAME_SURNAME= "SELECT * FROM clients WHERE name_surname = ?";
}
