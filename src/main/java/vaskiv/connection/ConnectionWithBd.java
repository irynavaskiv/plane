package vaskiv.connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import static vaskiv.consts.connectionConstants.*;
@SuppressWarnings("ALL")
public class ConnectionWithBd {

    private static ConnectionWithBd connectionWithBd = null;
    public static ConnectionWithBd getConnectionWithBd() {
        if (connectionWithBd == null) {
            try {
                connectionWithBd = (ConnectionWithBd) DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                System.out.println("SQLExeption" + e.getMessage());
                System.out.println("SQLState" + e.getSQLState());
                System.out.println("VendorError" + e.getErrorCode());
            }
            return connectionWithBd;
        }
        return null;
    }
    public Statement createStatement() {  return null;  }
    public PreparedStatement prepareStatement(String findId) {  return  null;  }
}
