
create schema if not exists project default character set utf8 ;
use project ;

create table if not exists plane(
id int auto_increment primary key,
name_plane text(30) not null,
motor int not null,
speed int not null)
ENGINE InnoDB;

create table if not exists flights (
id int auto_increment primary key,
trip int not null,
time_flights int not null,
number_flights int not null)
ENGINE InnoDB;

create table if not exists booking (
id int auto_increment primary key,
hour_booking int not null,
quantity int not null)
ENGINE InnoDB;


create table if not exists clients (
id int auto_increment primary key,
name_surname text(30) not null)
ENGINE InnoDB;


insert into plane (id, name_plane, motor, speed)
values (1,'Boeing' ,23,100),
       (2, 'Bell',23,100),
       (3,'Saab', 23,100),
       (4,'Airbus', 23,100);

insert into flights (id, trip, time_flights, number_flights)
values (5, 5, 3, 8),
       (6, 8, 4, 12),
       (7, 9, 5, 11),
       (8, 6, 6, 3);
			
insert into booking (id, hour_booking,quantity)
values (9,2,13),
	   (10,3,8),
       (11,2,1),
       (12,5,5);

insert into clients (id, name_surname)
values (13,'Ira Vaskiv'),
       (14,'Solomia Bohutska'),
       (15,'Yulia Bobak'),
	   (16,'Maria Halan');

            
